{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# <center> Harvesting and Analyzing Neighborhood Data from Social Media </center>\n",
    "\n",
    "<center>By Meng Cai <center>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"https://i.imgur.com/Qrwsyf1.png\" width=\"60%\">\n",
    "<p style=\"text-align: right;\">Image from: https://i.imgur.com/Qrwsyf1.png</p>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "# Authors\n",
    "\n",
    "Meng Cai"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "# Abstract\n",
    "\n",
    "The large quantity of textual data provides new opportunities for urban researchers to examine people’s perceptions, attitudes, and behaviors. NBHReviewCollector enables automated social media data collection and text analysis (including both social media data and survey data). It is a toolbox that combines together existing python libraries for urban researchers to analyze text data."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "----\n",
    "# Statement of Need\n",
    "\n",
    "The advancement of technologies is not just changing cities, it is transforming the way researchers are able to study cities. Conventional social data gathering techniques, such as surveys, focus groups, and interviews, are oftentimes expensive and time-consuming. Publicly accessible community information are only available at county or zip code level and are updated annually or once several years. Social media offer urban researchers a potential to draw community characteristics at a finer spatio-temporal scale with substantially lower cost. Abundant urban big data is being generated and stored at unprecedented speed and scale; researchers nowadays are able to ask and answer questions in ways that were impossible in the past.\n",
    "\n",
    "Among all existing data, 95% are in unstructured form, which lacks identifiable tabular organization required by traditional data analysis methods (Gandomi & Haider, 2015). Unstructured data, such as Web pages, may contain numerical information, but is usually text-heavy. The large quantity of textual data provides new opportunities for urban researchers to examine people’s perceptions, attitudes, and behaviors, so as to advance the knowledge and understanding of urban dynamics. \n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "----\n",
    "# Installation instructions\n",
    "\n",
    "Clone the git repo:\n",
    "\n",
    "    git clone https://gitlab.msu.edu/caimeng2/NBHReviewCollector.git\n",
    "    \n",
    "Set up a conda environment by running the following command:\n",
    "\n",
    "    conda env create --prefix ./envs --file environment.yml\n",
    "\n",
    "    conda activate ./envs\n",
    "    \n",
    "Alternatively, please install the following dependencies:\n",
    "    \n",
    "    TwitterAPI\n",
    "    pyLDAvis\n",
    "    textblob\n",
    "    nltk\n",
    "    scikit-learn\n",
    "    numpy\n",
    "    spacy\n",
    "    pandas\n",
    "    matplotlib\n",
    "    gensim\n",
    "    pylint\n",
    "    pdoc3\n",
    "    pytest\n",
    "\n",
    "Generate automatic documentation by running the following command:\n",
    "\n",
    "    pdoc --force --html --output-dir ./docs NBHReviewCollector"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "----\n",
    "# Unit Tests\n",
    "\n",
    "Unit tests are provided in the folder `./NBHReviewCollector/NBHReviewCollector/tests`. \n",
    "\n",
    "Test the software by running the following commend:\n",
    "\n",
    "    pytest NBHReviewCollector\n",
    "\n",
    "All of the test cases should pass. `test_imports.py` tests if the dependencies have been installed. Four functions are included in `TextProcessing.py`, which checks if importing survey data and tweets, and cleaning text data are working properly. Five functions are included in `test_ExploratoryAnalysis.py`, which tests whether tokenizing, lemmatization, processing tokens, preparing data for topic modeling, and topic modeling are working properly. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "----\n",
    "# Example usage\n",
    "\n",
    "An example that demonstrates how to use the software is included at `./NBHReviewCollector/example.ipynb`. Example data files could also be found in the root folder `./NBHReviewCollector`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "# Methodology\n",
    "\n",
    "Currently, there are four components in NBHReviewCollector: downloading tweets, plotting top words and phrases, finding salient topics, and detecting sentiments in text. The first part, `TweetCollector`, enables users to collect historical tweets (as early as 2006). It is built upon an existing module [TwitterAPI](https://github.com/geduldig/TwitterAPI/tree/master/TwitterAPI). \n",
    "\n",
    "The second component, `TextProcessing`, cleans and organizes survey data and tweets in a way that is suitable for further text analysis. It includes removing empty or NA answers, links, special characters, and numbers from text and convert time and location into a more readable format.\n",
    "\n",
    "`ExploratoryAnalysis` and `SortText` is the core of NBHReviewCollector. Counting and plotting the most frequent words and phrases help users to better understand the contents of the text. The function `LDAtopic` discovers the salient topics in text. It is done by Latent Dirichlet Allocation (LDA) topic modeling, which is a type of unsupervised machine learning. LDA considers a document as a bag of words and assumes that a document is a distribution of some topics and each topic is a distribution of some words. This function is built upon [Gensim](https://radimrehurek.com/gensim/), [nltk](https://www.nltk.org/), and [scikit-learn](https://scikit-learn.org/stable/). The topics detected by `LDAtopic` could be visualized by `PlotLDA`, which is built upon [pyLDAvis](https://pypi.org/project/pyLDAvis/).\n",
    "\n",
    "The last component of NBHReviewCollector is to identify sentiments in text. Existing libraries such as [TextBlob](https://textblob.readthedocs.io/en/dev/) only achieves about 60% accuracy compared to manual coding. `SortText` uses [Neural Networks classifiers](https://scikit-learn.org/stable/modules/generated/sklearn.neural_network.MLPClassifier.html) for sentiment analysis. The accuracy increases to 80%, which is comparable to the agreement rate between two human coders.\n",
    "\n",
    "NBHReviewCollector meets all the submission guidelines.\n",
    "- Bi-weekly reports could be found at `./NBHReviewCollector/Models`.\n",
    "- The original proposal is included at `./NBHReviewCollector/docs`.\n",
    "- The final report could be found at `./NBHReviewCollector/docs`.\n",
    "- The software could be found at `./NBHReviewCollector/NBHReviewCollector`.\n",
    "- Automatic documentations generated by `pdoc3` could be found at `./NBHReviewCollector/docs/NBHReviewCollector`.\n",
    "- A MIT license is included in the software root directory `./NBHReviewCollector`.\n",
    "- Unit tests are provided at `./NBHReviewCollector/NBHReviewCollector/tests`.\n",
    "- Example data and a Jupyter Notebook demonstrating how to use the software could be found at the root directory `./NBHReviewCollector`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "---\n",
    "# Concluding Remarks\n",
    "\n",
    "NBHReviewCollector improves the usability of urban data source. While this is exciting, it is important for users not to overestimate what machines are capable of doing and to acknowledge its limitations. While LDA topic modeling techniques provide quick and valuable information extracted from text, the modeling result interpretation still requires a lot of human intelligence.\n",
    "\n",
    "Ultimately, this software aims to collect texts from various online data sources and compare the results with gold-standard traditional surveys. Functions to scrape reviews from Niche.com and Nextdoor.com are to be added in the future."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "----\n",
    "# Mentions\n",
    "\n",
    "Mimi Gong adopted part of this software for her research in mangrove conservation."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "----\n",
    "# References\n",
    "\n",
    "Gandomi, A., & Haider, M. (2015). Beyond the hype: Big data concepts, methods, and analytics. International Journal of Information Management, 35(2), 137–144.\n",
    "\n",
    "[Gensim](https://radimrehurek.com/gensim/)\n",
    "\n",
    "[NBHReviewCollector](https://gitlab.msu.edu/caimeng2/NBHReviewCollector)\n",
    "\n",
    "[nltk](https://www.nltk.org/)\n",
    "\n",
    "[pyLDAvis](https://pypi.org/project/pyLDAvis/)\n",
    "\n",
    "[scikit-learn](https://scikit-learn.org/stable/)\n",
    "\n",
    "[TextBlob](https://textblob.readthedocs.io/en/dev/)\n",
    "\n",
    "[TwitterAPI](https://github.com/geduldig/TwitterAPI/tree/master/TwitterAPI)\n"
   ]
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.4"
  },
  "varInspector": {
   "cols": {
    "lenName": 16,
    "lenType": 16,
    "lenVar": 40
   },
   "kernels_config": {
    "python": {
     "delete_cmd_postfix": "",
     "delete_cmd_prefix": "del ",
     "library": "var_list.py",
     "varRefreshCmd": "print(var_dic_list())"
    },
    "r": {
     "delete_cmd_postfix": ") ",
     "delete_cmd_prefix": "rm(",
     "library": "var_list.r",
     "varRefreshCmd": "cat(var_dic_list()) "
    }
   },
   "types_to_exclude": [
    "module",
    "function",
    "builtin_function_or_method",
    "instance",
    "_Feature"
   ],
   "window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
