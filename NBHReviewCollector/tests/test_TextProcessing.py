from NBHReviewCollector import TextProcessing as tp
import pytest
import datetime

def test_ImportComment():
    data = tp.ImportComment("test_comment.csv", "feel")
    assert len(data) == 1

def test_TextCleaner():
    data = tp.TextCleaner("@MplsPedestrian yes, good!")
    assert data == "yes good"

def test_ImportTweet_location():
    data = tp.ImportTweet("test_tweet.csv")
    assert data["lat"].iloc[0] == 36.13024

def test_ImportTweet_tweet():
    data = tp.ImportTweet("test_tweet.csv")
    assert data["tweet"].iloc[0] == "yes good"
