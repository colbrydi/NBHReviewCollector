"""Sort text data into categories."""

from sklearn.feature_extraction.text import CountVectorizer
from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPClassifier


def SortText(text, training_data, category):
    """
    Train a Neural Networks classifier to sort text into pre-specified categories.

    Input:
    text -- list of text data to be sorted;
    training_data -- list of training text;
    category -- list of training labels.
    Output:
    category labels decided by sklearn.neural_network.

    Reference:
    https://scikit-learn.org/stable/modules/generated/sklearn.neural_network.MLPClassifier.html
    """
    text_train, text_test, y_train, y_test = train_test_split(training_data.values, category,
                                                              test_size=0.3, random_state=100)
    vectorizer = CountVectorizer(lowercase=True)
    vectorizer.fit(text_train)
    x_train = vectorizer.transform(text_train)
    x_test = vectorizer.transform(text_test)
    model = MLPClassifier(alpha=1, max_iter=1000)
    model.fit(x_train, y_train)
    x_new = vectorizer.transform(text)
    pred = []
    for xdata in x_new:
        pred.append(model.predict(xdata)[0])
    return pred
